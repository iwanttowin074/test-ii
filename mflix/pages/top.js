import clientPromise from '../lib/mongodb'

export default function Top({ movies }) {
    return (
        <div>
            <h1>Top 1000 Movies of All Time</h1>
            <p>
                <small>According to Something</small>
            </p>
            <ul>
                {movies.map( (movie) => (
                    <li>
                        <h2>{movie.original_title}</h2>
                        <h3>{movie.vote_average}</h3>
                        <p>{movie.overview}</p>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export async function getStaticProps() {
    const client = await clientPromise
    const db = await client.db('movies')

    const movies = await db
        .collection('movies_metadata')
        .find({})
        .sort({ vote_average : -1 })
        .limit( 1000 )
        .toArray()
    
    return {
        props : {
            movies : JSON.parse( JSON.stringify(movies) )
        }
    }
}